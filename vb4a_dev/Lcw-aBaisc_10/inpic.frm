VERSION 5.00
Begin VB.Form inpic 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   2895
   ClientLeft      =   10035
   ClientTop       =   6720
   ClientWidth     =   6135
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2895
   ScaleWidth      =   6135
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   4080
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   120
      Width           =   1935
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   495
      Left            =   4080
      TabIndex        =   1
      Top             =   1680
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   4080
      TabIndex        =   0
      Top             =   2280
      Width           =   1935
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   2655
      Left            =   120
      Stretch         =   -1  'True
      Top             =   120
      Width           =   3855
   End
End
Attribute VB_Name = "inpic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public insEnable As Boolean
Public Pinpos As Long



Private Sub Combo1_Click()
    On Error Resume Next
    'GdipLoadImageFromFile strptr("c:\aaa.png"),img
    
    Image1.Picture = LoadPicture(App.Path & "\" & Form1.Text6.text & "\preview\" & Mid(Combo1.text, 1, Len(Combo1.text) - 3) & "jpg")
    'GdipLoadImageFromFile StrPtr(App.Path & "\" & Form1.Text6.text & "\assets\" & Combo1.text), Image1
End Sub

Private Sub Command1_Click()
    On Error GoTo ErrHand

    Form3.Str2add = Chr(34) & Trim(Combo1.text) & Chr(34)
    Call Form3.refreshText2(Pinpos)
    Me.Hide

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    
    If Form1.CH Then
        Command1.Caption = "插入"
        Command2.Caption = "确定"
    Else
        Command1.Caption = "Insert"
        Command2.Caption = "OK"
    End If
    
    piclist = FreeFile()
    
    Dim MyFile As String, strFile As String
    
    Open App.Path & "\" & Form1.Text6.text & "\pic.list" For Output As #piclist
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.jpg"
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.bmp"
    
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.png"
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.gif"
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    Close piclist
    
    'If Dir(App.Path & "\" & Form1.Text6.text & "\pic.list") = "" Then
    '    If Form1.CH Then
    '    MsgBox "当前工程未发现图片 ！", vbCritical, ""
    '    Else
    '    MsgBox "No picture found in current project!", vbCritical, ""
    '    End If
    'insEnable = False
    
    'End If
    
    'Command1.Enabled = insEnable
    
    Combo1.Clear
    
    Open App.Path & "\" & Form1.Text6.text & "\pic.list" For Input As #1
    
    If LOF(1) = 0 Then
        If Form1.CH Then
            MsgBox "当前工程未发现图片！", vbCritical, ""
        Else
            MsgBox "No picture found in current project!", vbCritical, ""
        End If
        
        Close #1
        
        Me.Hide
    Else
        
        If Dir(App.Path & "\" & Form1.Text6.text & "\preview", vbDirectory) = "" Then MkDir App.Path & "\" & Form1.Text6.text & "\preview"
        
        Do While Not EOF(1)
            
            Line Input #1, tmpstr
            Combo1.AddItem (tmpstr)
            Dim Convstr As String
            
            Convstr = App.Path & "\lib\bin\nconvert.exe -out jpeg -resize 385 265 -o " & App.Path & "\" & Form1.Text6.text & "\preview\" & Mid(tmpstr, 1, Len(tmpstr) - 3) & "jpg" & " " & App.Path & "\" & Form1.Text6.text & "\assets\" & tmpstr
            
            Debug.Print Convstr
            
            If Dir(App.Path & "\" & Form1.Text6.text & "\preview\" & Mid(tmpstr, 1, Len(tmpstr) - 3) & "jpg") = "" Then
                Call Form1.Holdrun(Convstr, True)
            End If
            
        Loop
        
        Close #1
        
    End If
    
    If insEnable = False Then Me.Hide
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
