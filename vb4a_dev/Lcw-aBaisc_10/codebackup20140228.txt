Private Sub Text1_Change()
Static RunTime
If RunTime = 1 Then GoTo 退出
RunTime = 1
If 编辑状态 = 0 Then GoTo 退出
Dim pos As Long
Dim StrEXT As String
Dim StrTXT As String
Dim POSTXT As Long
Dim ifixStr As String

If Shift值 = 2 And KeyCode值 = vbKeyV Then Call 大面积改动(上一次操作的位置, Text1.SelStart): GoTo 退出
If KeyCode值 = 17 Then GoTo 退出
Dim LastPos, StaPos, EndPos, KeyWord, Key
If KeyCode = 37 Then LastPos = Text1.SelStart Else LastPos = Text1.SelStart
Text1.UpTo " ,.?!:()=" & Chr(10), True, False
EndPos = Text1.SelStart
Text1.UpTo " ,.?!:()=" & Chr(10), False, False
StaPos = Text1.SelStart
Text1.SelLength = EndPos - StaPos
Call 判断列表
Text1.SelLength = 0
Text1.SelStart = LastPos
操作前的内容 = Text1.text



    If Check1.Value = 0 Then Exit Sub
     inComp = False
    If KeyCode值 = 190 Or KeyCode值 = 110 Then
        
        If Mid(Text1.text, Text1.SelStart, 1) = "." Then

            Call AutoComplete

            inComp = True

        End If

    End If
Shift值 = 0
KeyCode值 = 0
编辑状态 = 0
RunTime = 0
Exit Sub
退出:
Shift值 = 0
KeyCode值 = 0
编辑状态 = 0
RunTime = 0
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)
上一次操作的位置 = Text1.SelStart
KeyCode值 = KeyCode
Shift值 = Shift
编辑状态 = 1
End Sub



Sub 大面积改动(开始位置, 结束位置)                    '粘贴了代码后调用这个来保持高亮，我在CTRL+V的键盘事件里已经预置了...你可以忽略...
Text1.Enabled = False
Dim Lengh
Text1.SelStart = 0
For i = 开始位置 To 结束位置
Text1.SelStart = i
Text1.UpTo " ,.?!:()=" & Chr(10), True, False
EndPos = Text1.SelStart
Text1.UpTo " ,.?!:()=" & Chr(10), False, False
StaPos = Text1.SelStart
Text1.SelLength = EndPos - StaPos
Call 判断列表
Text1.SelLength = 0
Next i
Text1.Enabled = True
End Sub

Public Sub 装载代码()                                         '打开新工程后，代码加载完毕了调用这个过程...暂时没思路了，这个代码效率非常低...
Dim tagname1 As String
Picture1.Visible = True
btnCan.Visible = True
Combo1.Enabled = False


tagname1 = Me.Caption
Text1.Enabled = False
Dim Lengh
Text1.SelStart = 0
For i = 0 To Len(Text1.text)
Text1.SelStart = i
Text1.UpTo " ,.?!:()=" & Chr(10), True, False
EndPos = Text1.SelStart
Text1.UpTo " ,.?!:()=" & Chr(10), False, False
StaPos = Text1.SelStart
Text1.SelLength = EndPos - StaPos
Call 判断列表
Text1.SelLength = 0
DoEvents
Me.Caption = "Loading..." & "(" & Int(i * 100 / Len(Text1.text)) & "%)"
If IMCANCELED = True Then
Me.Caption = tagname1
Combo1.Enabled = True
btnCan.Visible = False
Picture1.Visible = False
Exit For
End If
Next i
Text1.Enabled = True
Me.Caption = tagname1
Combo1.Enabled = True
btnCan.Visible = False
Picture1.Visible = False
End Sub

Sub 判断列表()                                           '虽然代码很长，但可以扩展功能，比如加粗，都可以实现的。因此，我放弃使用array数组了。
Select Case LCase(Me.Text1.SelText)                  '这里是关键字列表...
Case "split", "boolean", "byte", "short", "integer", "long", "single", "double", "string", "date", "object", "variant", "true", "false", "format", "alias", "and", "not", "next", "as", "case", "const", "dim", "each", "else", "end", "error", "event", "exit", "for", "function", "get", "if", "in", "is", "like", "me", "static", "step", "sub", "then", "to", "until", "while", "xor", "new", "on", "available", "enabled", "text", "value", "hint", "acc", "quit", "hide", "ceil", "asin", "acos", "floor", "round", "int", "abs", "cos", "exp", "log", "max", "min", "rnd", "sin", "sgn", "sqr", "tan", "unpack", "day", "hour", "minute", "month", "now", "second", "timer", "weekday", "year", "left", "len", "mid", "replace", "right", "trim", "msgbox", "asc", "hex", "chr", "finish", "str", "val", "select"
'关键字列表1
Text1.SelColor = RGB(0, 0, 255)                   '可以扩充这一行加上其他功能...比如加宽斜体...
Key = 1                                                  '这个不要动...
Text1.SelText = UCase(Mid(Text1.SelText, 1, 1)) & LCase(Mid(Text1.SelText, 2, Len(Text1.SelText) - 1))                            '这里的是规范写法...注意自动更正必须放在最后...

Case "elseif"
Text1.SelColor = RGB(0, 0, 255)
Key = 1
Text1.SelText = "ElseIf"

Case "lcase", "ltrim", "rtrim", "ucase"
Text1.SelColor = RGB(0, 0, 255)
Key = 1
Text1.SelText = UCase(Mid(Text1.SelText, 1, 2)) & LCase(Mid(Text1.SelText, 3, Len(Text1.SelText) - 2))

Case "files", "application", "math", "dates", "strings", "conversions"
Text1.SelColor = RGB(0, 255, 255)
Key = 1
Text1.SelText = UCase(Mid(Text1.SelText, 1, 1)) & LCase(Mid(Text1.SelText, 2, Len(Text1.SelText) - 1))

Case "gps"
Text1.SelColor = RGB(100, 100, 255)
Key = 1
Text1.SelText = "GPS"

Case "myphone"
Text1.SelColor = RGB(100, 100, 255)
Key = 1
Text1.SelText = "MyPhone"

Case "mytimer"
Text1.SelColor = RGB(100, 100, 255)
Key = 1
Text1.SelText = "MyTimer"

Case "orient", "mytimer", "acc"
Text1.SelColor = RGB(100, 100, 255)
Key = 1
Text1.SelText = UCase(Mid(Text1.SelText, 1, 1)) & LCase(Mid(Text1.SelText, 2, Len(Text1.SelText) - 1))

Case "sendsms"
Key = 1
Text1.SelText = "SendSMS"

Case "sendmail"
Key = 1
Text1.SelText = "SendMail"

Case "socketsend"
Key = 1
Text1.SelText = "SocketSend"

Case "jumpurl"
Key = 1
Text1.SelText = "JumpURL"

Case "currentaddress"
Key = 1
Text1.SelText = "CurrentAddress"

Case "orientationchanged"
Key = 1
Text1.SelText = "OrientationChanged"

Case "accelerationchanged"
Key = 1
Text1.SelText = "AccelerationChanged"

Case "xaccel"
Key = 1
Text1.SelText = "XAccel"

Case "yaccel"
Key = 1
Text1.SelText = "YAccel"

Case "zaccel"
Key = 1
Text1.SelText = "ZAccel"

Case "picture"
Key = 1
Text1.SelText = "Picture"

Case "initialize", "available", "call", "vibrate", "changed", "latidude", "longitude", "altidude", "accuracy", "enable", "yaw", "pitch", "roll", "angle", "magnitude", "timer", "interval", "shaking", "available"
Key = 1
Text1.SelText = UCase(Mid(Text1.SelText, 1, 1)) & LCase(Mid(Text1.SelText, 2, Len(Text1.SelText) - 1))

LastPos = LastPos + 1

Case Else
If Key = 1 Then
Text1.SelColor = RGB(0, 0, 255)
Else
Text1.SelColor = RGB(0, 0, 0)
End If
End Select
End Sub

Private Sub Text1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 2 Then PopupMenu Edit
End Sub

Private Sub 复制_Click()
If Text1.SelText = "" Then Exit Sub
Clipboard.SetText Text1.SelText
End Sub

Private Sub 剪切_Click()
If Text1.SelText = "" Then Exit Sub
Clipboard.SetText Text1.SelText
Text1.SelText = ""
End Sub

Private Sub 粘贴_Click()
Dim Before
Before = Text1.SelStart
Text1.SelText = Clipboard.GetText
Call 大面积改动(Before, Text1.SelStart)
End Sub
