VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form colorPicker 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "颜色选择"
   ClientHeight    =   1935
   ClientLeft      =   9360
   ClientTop       =   6885
   ClientWidth     =   3975
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1935
   ScaleWidth      =   3975
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "自定义"
      Height          =   1695
      Left            =   3000
      TabIndex        =   12
      Top             =   120
      Width           =   855
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H80000008&
         Height          =   1335
         Index           =   12
         Left            =   120
         ScaleHeight     =   1305
         ScaleWidth      =   585
         TabIndex        =   16
         Top             =   240
         Width           =   615
      End
      Begin VB.HScrollBar HScroll1 
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   480
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label Label2 
         Caption         =   "255"
         Height          =   255
         Left            =   1200
         TabIndex        =   14
         Top             =   240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "ALPHA:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Visible         =   0   'False
         Width           =   615
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2520
      Top             =   1920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   11
      Left            =   2280
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   11
      Top             =   1320
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   10
      Left            =   1560
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   10
      Top             =   1320
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   9
      Left            =   840
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   9
      Top             =   1320
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FF80FF&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   8
      Left            =   120
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   8
      Top             =   1320
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   7
      Left            =   2280
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   7
      Top             =   720
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00008000&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   6
      Left            =   1560
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   6
      Top             =   720
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFF80&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   5
      Left            =   840
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   5
      Top             =   720
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FF0000&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   4
      Left            =   120
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   4
      Top             =   720
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   3
      Left            =   2280
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   3
      Top             =   120
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   2
      Left            =   1560
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   2
      Top             =   120
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   1
      Left            =   840
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   1
      Top             =   120
      Width           =   615
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   0
      Left            =   120
      Picture         =   "Form8.frx":0000
      ScaleHeight     =   465
      ScaleWidth      =   585
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "colorPicker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Comefrom As Integer
Public ColorStr As String
Dim Alpha As String

Private Sub Form_Load()
    On Error GoTo ErrHand

    If Form1.CH Then
        Frame1.Caption = "其他颜色"
    Else
        Frame1.Caption = "Others"
    End If
    HScroll1.Max = 255
    HScroll1.Min = 0
    HScroll1.value = 255

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub

Private Sub HScroll1_Change()
    On Error GoTo ErrHand

    Label2.Caption = HScroll1.value

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "HScroll1_Change", vbExclamation
End Sub

'"COLOR_NONE"
    

'"COLOR_BLACK"
'"COLOR_DKGRAY"
'"COLOR_GRAY"
'"COLOR_BLUE"
'"COLOR_CYAN"
'"COLOR_GREEN"
'"COLOR_LTGRAY"
'"COLOR_MAGENTA"
'"COLOR_RED"
'"COLOR_WHITE"
'"COLOR_YELLOW"
Private Sub Picture1_Click(Index As Integer)
    
    Select Case Index
        
    Case 0
        ColorStr = "Component.COLOR_NONE"
        
    Case 1
        ColorStr = "Component.COLOR_BLACK"
        
    Case 2
        ColorStr = "Component.COLOR_DKGRAY"
        
    Case 3
        ColorStr = "Component.COLOR_GRAY"
        
    Case 4
        ColorStr = "Component.COLOR_BLUE"
        
    Case 5
        ColorStr = "Component.COLOR_CYAN"
        
    Case 6
        ColorStr = "Component.COLOR_GREEN"
        
    Case 7
        ColorStr = "Component.COLOR_LTGRAY"
        
    Case 8
        ColorStr = "Component.COLOR_MAGENTA"
        
    Case 9
        ColorStr = "Component.COLOR_RED"
        
    Case 10
        ColorStr = "Component.COLOR_WHITE"
        
    Case 11
        ColorStr = "Component.COLOR_YELLOW"
        
    Case 12
        
        Me.CommonDialog1.ShowColor
        Picture1(12).BackColor = CommonDialog1.color
        Alpha = Format(Hex(HScroll1.value), "00")
        
        ColorStr = "&H" & Alpha & Mid(Replace(Format(Hex$(CommonDialog1.color), "@@@@@@"), " ", "0"), 5, 2) & Mid(Replace(Format(Hex$(CommonDialog1.color), "@@@@@@"), " ", "0"), 3, 2) & Mid(Replace(Format(Hex$(CommonDialog1.color), "@@@@@@"), " ", "0"), 1, 2)
        
        Debug.Print ColorStr; "-"; CommonDialog1.color
        
    Case Else
        
    End Select
    
    Select Case Comefrom
    Case 1
        
        Form1.Label12.ForeColor = Me.Picture1(Index).BackColor
        Form1.InternalTC = ColorStr
        Form1.Picture3.BackColor = Me.Picture1(Index).BackColor
    Case 0
        
        Form1.Label3.BackColor = Me.Picture1(Index).BackColor
        Form1.Label12.BackColor = Me.Picture1(Index).BackColor
        Form1.color = ColorStr
        
    Case 2
        Form1.Label17.BackColor = Me.Picture1(Index).BackColor '
        VB4AModule.FMBGColor(Form1.Combo3.ListIndex + 1) = ColorStr
        Open App.Path + "\" + Form1.Text6.text + "\bgc.dat" For Output As #3
        
        Print #3, Join(VB4AModule.FMBGColor(), "|")
        
        Close #3
    Case 3
        Form3.Str2add = ColorStr
        
        Call Form3.refreshText2(Form3.Text1.SelStart)
        Form3.Text1.SetFocus
        
        
        
    End Select
    
    Unload Me
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Picture1_Click", vbExclamation
End Sub
